# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import csv
import os 

class Edeka24Pipeline:
    def process_item(self, item, spider):
        return item



class CsvPipeline:

    def open_spider(self, spider):
        output_dir = 'output'
        os.makedirs(output_dir, exist_ok=True)
        self.file = open('output/products.csv', 'w', newline='', encoding='utf-8')
        self.writer = csv.writer(self.file)
        self.writer.writerow(['title', 'link', 'image_url', 'price', 'price_per_kg', 'bio_label'])

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        self.writer.writerow([item['title'], item['link'], item['image_url'], item['price'], item['price_per_kg'], item['bio_label']])
        return item
