import scrapy
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import chromedriver_autoinstaller
from edeka24.items import ProductItem  # Make sure to replace with the actual import

class ProductSpider(scrapy.Spider):
    name = "products"
    start_urls = ['https://www.edeka24.de/Lebensmittel/Suess-Salzig/Schokoriegel/']  # Replace with the actual URL
    
    def start_requests(self):
        chromedriver_autoinstaller.install()
        yield SeleniumRequest(url=self.start_urls[0], callback=self.parse)

    def parse(self, response):
        driver = response.meta['driver']
        while True:
            try:
                # Extract product details before clicking load more
                for product in driver.find_elements(By.CSS_SELECTOR, 'div.product-item'):
                    item = ProductItem()
                    item['title'] = product.find_element(By.CSS_SELECTOR, 'a.title h2').text.strip()
                    item['link'] = product.find_element(By.CSS_SELECTOR, 'a.title').get_attribute('href')
                    item['image_url'] = product.find_element(By.CSS_SELECTOR, 'div.product-image a img').get_attribute('src')
                    item['price'] = product.find_element(By.CSS_SELECTOR, 'div.price').text.strip()
                    item['price_per_kg'] = product.find_element(By.CSS_SELECTOR, 'p.price-note').text.strip()
                    item['bio_label'] = 'Bio' if product.find_elements(By.CSS_SELECTOR, 'ul.characteristics li.bio') else None

                    yield item

                # Wait for the loader button to be clickable and click it
                wait = WebDriverWait(driver, 10)
                load_more_button = wait.until(EC.element_to_be_clickable((By.ID, 'loader-btn')))
                load_more_button.click()

                # Wait for the loader to disappear
                wait.until_not(EC.presence_of_element_located((By.ID, 'loader-icon')))

                # Check if the end of the list is reached
                end_button = driver.find_element(By.ID, 'loader-btn')
                if 'endlist' in end_button.get_attribute('class'):
                    break
            except Exception as e:
                self.logger.error("Error during loading more products: %s", e)
                break

        # One final extraction of products after the last button click
        for product in driver.find_elements(By.CSS_SELECTOR, 'div.product-item'):
            item = ProductItem()
            item['title'] = product.find_element(By.CSS_SELECTOR, 'a.title h2').text.strip()
            item['link'] = product.find_element(By.CSS_SELECTOR, 'a.title').get_attribute('href')
            item['image_url'] = product.find_element(By.CSS_SELECTOR, 'div.product-image a img').get_attribute('src')
            item['price'] = product.find_element(By.CSS_SELECTOR, 'div.price').text.strip()
            item['price_per_kg'] = product.find_element(By.CSS_SELECTOR, 'p.price-note').text.strip()
            item['bio_label'] = 'Bio' if product.find_elements(By.CSS_SELECTOR, 'ul.characteristics li.bio') else None

            yield item
