# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class ProductItem(scrapy.Item):
    title = scrapy.Field()
    link = scrapy.Field()
    image_url = scrapy.Field()
    price = scrapy.Field()
    price_per_kg = scrapy.Field()
    bio_label = scrapy.Field()

