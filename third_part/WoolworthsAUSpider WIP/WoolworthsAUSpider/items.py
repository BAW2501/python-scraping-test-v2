# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class ProductItem(scrapy.Item):
    title = scrapy.Field()
    link = scrapy.Field()
    image_url = scrapy.Field()
    current_price = scrapy.Field()
    was_price = scrapy.Field()
    price_per_unit = scrapy.Field()
    promotion = scrapy.Field()
    promotion_label = scrapy.Field()
    promotion_save_amount = scrapy.Field()
    sponsored = scrapy.Field()
