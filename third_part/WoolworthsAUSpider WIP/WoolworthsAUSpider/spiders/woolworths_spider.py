import scrapy
from scrapy_selenium import SeleniumRequest
from WoolworthsAUSpider.items import ProductItem
import chromedriver_autoinstaller
from scrapy.utils.response import open_in_browser



class WoolworthsSpider(scrapy.Spider):
    name = "woolworths"

    def start_requests(self):
        chromedriver_autoinstaller.install()
        
        url = "https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas"
        headers ={
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
        "accept-language": "en-US,en;q=0.9,fr-FR;q=0.8,fr;q=0.7,ar;q=0.6",
        "cache-control": "max-age=0",
        "if-none-match": "W/\"15749c-HuzhtFhfXRTdo90Z3d574Obzb5M\"",
        "priority": "u=0, i",
        "sec-ch-ua": "\"Google Chrome\";v=\"125\", \"Chromium\";v=\"125\", \"Not.A/Brand\";v=\"24\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "cookie": "ai_user=JVYGwYcFheF6YRWAmh1626|2024-06-02T03:04:42.415Z; EnableLandingPageVideosContentService=true; EnableLandingPageIdeasContentService=true; at_check=true; AMCVS_4353388057AC8D357F000101%40AdobeOrg=1; INGRESSCOOKIE=1717359252.39.1279.490468|37206e05370eb151ee9f1b6a1c80a538; s_cc=true; akaalb_woolworths.com.au=~op=www_woolworths_com_au_BFF_MEL_Launch:WOW-BFF-MEL|www_woolworths_com_au_ZoneB:PROD-ZoneB|~rv=80~m=WOW-BFF-MEL:0|PROD-ZoneB:0|~os=43eb3391333cc20efbd7f812851447e6~id=13dc0ea9799bdb7464b87ad4f8ac3d8f; bm_mi=8AF1F6C86FE17BDC166EE52A213EE3DC~YAAQzm1lXzKtGsePAQAA+53b2heyx5HQpZJopwwkqGR55zIkBK+5cGLPTAQixnKl6MXoZ2AZiaM3VxuawU62Y4nt+QI571cGRSzb3q75c/NQ28WQEURTT4zN5ZwCqFuWtcX8jqSUagBA3V47Fl3p/kZU4+uhMpEvMwmY4GmMHWk+s6VB2U6uBmFLwtW+jPLxxzRyvh0Iok7qr3y8SHwJhvG/X3QsUaEI3NxaxNkCXIwBLQ8tntQqhJjBXPxqAmNTkS/HtYJIw1PpDSDSdjqgN6mFLmb8/XQhWzO1CiFFcZ6QH+jnF6raurcRC36FFN3Gd2D4047/ZlS9BQySNcdzsbNwzXWqqYmIfFrpL8lvGTeS5yZOihaz8HmaXO0TA3meXIMJHfMpH1C/5FU=~1; fullstoryEnabled=false; bm_sv=55451E2F9FDBC8C1A6DFD077F3C95998~YAAQzm1lX7XQGsePAQAAjtrb2he9P4Gpe0Ws8fHEo60xaVwODnvXmQZdlunPeJuexJIw9W+ZPr6/+OYVIu+adyGam3feJ5o4JT8Zda0DDocJMbj9PY4k3lng+AIeYvLW5uupiXTFglThptPl3sEbY0ZF6lB76nNMqcIaYjMA+FqxcAPvlYwM920q5zTSN/bHPwYBFmmyf9+J5iHkxrET7VM87T7LBbyoKRR6FKJkqoK2v/OJByTaSfKnLpVJyCSbJZ0VEerskes=~1; w-rctx=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE3MTczNjU1ODUsImV4cCI6MTcxNzM2OTE4NSwiaWF0IjoxNzE3MzY1NTg1LCJpc3MiOiJXb29sd29ydGhzIiwiYXVkIjoid3d3Lndvb2x3b3J0aHMuY29tLmF1Iiwic2lkIjoiMCIsInVpZCI6IjljOTA0ZGQwLWZlMDItNDMxYy1hMDEwLWE5ZTQ4MzRiMTk1NiIsIm1haWQiOiIwIiwiYXV0IjoiU2hvcHBlciIsImF1YiI6IjAiLCJhdWJhIjoiMCIsIm1mYSI6IjEifQ.PsQS0Wy2poOoiWlD263YvIpIyP1l6DozyDGQkDQ_qwQ0O19O8E8dxGcBnBv_gJ5bdWNcwWR8rMCO46vdMfu42blf7DnmfmwS-nYIYCTQzhIML1BckrNWU6C-M7xQ62kno7KMp_UejdV3eOvY1Pzhb5LLDf16PBj8gN4Das_1P0aG1_JFcUWJhGOn30fh9duf8tTrDpPDtDgjAfOQK890OBSRKpPkA7PbPxZg8muOAJsoAWDyAFThtN7nYevUjKfYT_ZslZ1-Y0V1vqyrRJTETeSRC2N1nRcyaIuKLHXPrbjRXTM2pdq4zaSnAbhdUURviXqxPlIX7wBaG82DjgzUog; wow-auth-token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE3MTczNjU1ODUsImV4cCI6MTcxNzM2OTE4NSwiaWF0IjoxNzE3MzY1NTg1LCJpc3MiOiJXb29sd29ydGhzIiwiYXVkIjoid3d3Lndvb2x3b3J0aHMuY29tLmF1Iiwic2lkIjoiMCIsInVpZCI6IjljOTA0ZGQwLWZlMDItNDMxYy1hMDEwLWE5ZTQ4MzRiMTk1NiIsIm1haWQiOiIwIiwiYXV0IjoiU2hvcHBlciIsImF1YiI6IjAiLCJhdWJhIjoiMCIsIm1mYSI6IjEifQ.PsQS0Wy2poOoiWlD263YvIpIyP1l6DozyDGQkDQ_qwQ0O19O8E8dxGcBnBv_gJ5bdWNcwWR8rMCO46vdMfu42blf7DnmfmwS-nYIYCTQzhIML1BckrNWU6C-M7xQ62kno7KMp_UejdV3eOvY1Pzhb5LLDf16PBj8gN4Das_1P0aG1_JFcUWJhGOn30fh9duf8tTrDpPDtDgjAfOQK890OBSRKpPkA7PbPxZg8muOAJsoAWDyAFThtN7nYevUjKfYT_ZslZ1-Y0V1vqyrRJTETeSRC2N1nRcyaIuKLHXPrbjRXTM2pdq4zaSnAbhdUURviXqxPlIX7wBaG82DjgzUog; prodwow-auth-token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE3MTczNjU1ODUsImV4cCI6MTcxNzM2OTE4NSwiaWF0IjoxNzE3MzY1NTg1LCJpc3MiOiJXb29sd29ydGhzIiwiYXVkIjoid3d3Lndvb2x3b3J0aHMuY29tLmF1Iiwic2lkIjoiMCIsInVpZCI6IjljOTA0ZGQwLWZlMDItNDMxYy1hMDEwLWE5ZTQ4MzRiMTk1NiIsIm1haWQiOiIwIiwiYXV0IjoiU2hvcHBlciIsImF1YiI6IjAiLCJhdWJhIjoiMCIsIm1mYSI6IjEifQ.PsQS0Wy2poOoiWlD263YvIpIyP1l6DozyDGQkDQ_qwQ0O19O8E8dxGcBnBv_gJ5bdWNcwWR8rMCO46vdMfu42blf7DnmfmwS-nYIYCTQzhIML1BckrNWU6C-M7xQ62kno7KMp_UejdV3eOvY1Pzhb5LLDf16PBj8gN4Das_1P0aG1_JFcUWJhGOn30fh9duf8tTrDpPDtDgjAfOQK890OBSRKpPkA7PbPxZg8muOAJsoAWDyAFThtN7nYevUjKfYT_ZslZ1-Y0V1vqyrRJTETeSRC2N1nRcyaIuKLHXPrbjRXTM2pdq4zaSnAbhdUURviXqxPlIX7wBaG82DjgzUog; ak_bmsc=9535D20D478A1043F3C7A2CD56015E95~000000000000000000000000000000~YAAQxW1lX2aQlruPAQAA+Df32henPJVQLUwBd25vDtBr+9DbjUICnpYlEWe9g+uKyM/8Y06+o6bvxFxlpNwqVsA1nbZR8+wZYNRceUBE1l4s9t6Nm6KagPtJKaLhUGKJOZvFT0jqnp8p2r5jv8E6E5LqBCkbpAogbbvAev0lkVhoJZOPUU3GykMoqe/1C0Qs2oI/fI/T/Giu6pnY4lrJ+nQ1I96F+e5kVYLB6dBwJ0IgIx4fzqnEQ7Orkqt6z4BzMTedgNRRExXns+WqD/+CDmb3EvAm6tqMnZslKm14Atbc/YzjoiDsCmGL7q844Jp28VCD23oEvfxfKTPX817mPsqjMCgYHeQRus83mMVzPC5fSdbQ4G/2Poj4e1O52YundXO4ly/BLSSgfigHVQAlrwQdTVANeR69jxFj4QSHylURLv/mUCSCB8aeRrAjJNegdNaFEUidMcslTjAuApFeK+U+0ceFw/enTAWhEFpnlnXWb5gkqDHFllZUXyo=; ai_session=qtk6gehrKI7/e+ZsnMo8cK|1717359250785|1717366362916; _abck=AC8F61D74DA96690D98BBD163C06D098~0~YAAQjzwSAv/lN9qPAQAA8xwD2wvebqRX/6ZnTffT6AH89dw4EMRz+nJpFBx1+14w1A4WSyTik4eDZYzNbRDZkTHoW+Q3KU1/c0XwKIcbok78gVXysJA07izO+emCZin1Wc7zSNBBfMHhNidUb52racDmxacxawFNPXe9XpMl61wpf5tFI1t8qoNRwTc59AVrEWjpvBsCoYLkjs3f5dYRJWxOKhZEdm6zmlbvxPIr7mLsLoKR1MtmW0HBRfRtwnxkiK7YEHaVgW2JFdBn9MVty8Wn0wVHzmfckSTr5XJf06H+OBxAPcuR5n1rWhF8vZEWDQfwVPPK6WhTCUyc1AAk9aAqMLAa9Vi5CB2altk1HDI3YJtS79TkWloc92CzXvVMRGuYj4y5KEOljdEoi3OHKcRQgWWFl6AiNE/TNwTIvA==~-1~-1~1717367385; bm_sz=37BE754FCD68A49817A3E63F1C557B87~YAAQjzwSAgDmN9qPAQAA8xwD2xdtFrIdjlFaD6Bg4SqvyU0JbQDAFkkt93WyHxqt3P1GPLIT9a1Hd7G1aHQwbog4QzBnnNvi9f2oV7kg6Hv3rA6fgZPWXnxdZKCCf+9Hpgpz1gmiFUBePTx8oapxCzDk8zaUuVingAIeWpJ6EdANdKzd73XVWluptxyE4CARd+yITIN6EQ3fVTRrDkjq11g3uN81k12kLA2OnC+BBZYb0pX9fVCI/F+FP1FT5FwoWuFqLwpwF+LDqUrP/+zZAO/+lqOaM94nvo4MAkTpPbg8vHWXjvNjTAqma6OOEGrXCVt5N7lapavC0qYZzy6+YAij5cTBLByrihCpYCXcQxq3wqU4MQGD4Dw1Fx4r91N9jf53qhEyqzv5yCEUfPkcCditU1rGCIT3IPr87JqZu5er7+zCUGvmJdy3xRy/BjNzCX0T+4VXiKokh+H5X8R/J6sap9QQoGkhfmM=~4272944~4274233; mbox=session#f4c1767a11984a19bb973673ddf29006#1717368227; utag_main=v_id:018fd6e813250020636d0a8a02140506f005406700978$_sn:4$_se:4$_ss:0$_st:1717368166301$vapi_domain:woolworths.com.au$dc_visit:4$ses_id:1717366360164%3Bexp-session$_pn:2%3Bexp-session$dc_event:1%3Bexp-session; AMCV_4353388057AC8D357F000101%40AdobeOrg=179643557%7CMCIDTS%7C19877%7CMCMID%7C73138433084548452648458781821095093751%7CMCOPTOUT-1717373566s%7CNONE%7CvVersion%7C5.5.0"
    }
        yield SeleniumRequest(url=url, callback=self.parse,headers=headers)
    def parse(self, response):
        open_in_browser(response)
        for product in response.xpath('//wc-product-tile'):
            item = ProductItem()
            
            item['title'] = product.xpath('.//div[@class="title"]/a/text()').get()
            item['link'] = response.urljoin(product.xpath('.//div[@class="title"]/a/@href').get())
            item['image_url'] = product.xpath('.//div[@class="product-tile-image"]/a/img/@src').get()
            item['current_price'] = product.xpath('.//div[@class="product-tile-price"]/div[@class="primary"]/text()').get()
            item['was_price'] = product.xpath('.//span[@class="was-price"]/text()').get()
            item['price_per_unit'] = product.xpath('.//span[@class="price-per-cup"]/text()').get()
            item['promotion'] = product.xpath('.//div[@class="product-tile-label special"]/text()').get()
            item['promotion_label'] = product.xpath('.//div[@class="product-tile-label special"]/text()').get()
            item['promotion_save_amount'] = product.xpath('.//div[@class="product-tile-label special"]/span[@class="save-price"]/text()').get()
            item['sponsored'] = bool(product.xpath('.//div[@class="sponsored-text"]/text()').get())
            yield item