import gzip
import json
import pprint

def parse_categories():
    categories = []

    def get_categories_recursive(data):
        if isinstance(data, dict):
            if "content" in data and "href" in data["content"] and "label" in data["content"]:
                new_category = []
                content = data["content"]
                for cat in content['href'].replace('-',' ').split('/'):
                    if cat and cat not in new_category:
                        new_category.append(cat.title())
                new_category.append(content['label'])
                categories.append(new_category)
            for  value in data.values():
                get_categories_recursive(value)
        elif isinstance(data, list):
            for item in data:
                get_categories_recursive(item)

    with gzip.open("categories.json.gz", "rt") as f:
        input_dict = json.load(f)
        get_categories_recursive(input_dict)

    return categories

if __name__ == "__main__":
    pprint.pprint(parse_categories())
    # save the output to a file
    
