### Task1:
from typing import Generator
from collections import defaultdict
import re
from pprint import pprint


def get_substrings(s: str) -> Generator[str, None, None]:
    """
    This function yields all the possible substrings of a given string of length > 1
    :param s: str: input string
    """
    for i in range(2, len(s) + 1):
        for j in range(len(s) - i + 1):
            yield s[j : j + i]


def get_most_frequent_substring(words: list[str]) -> str:
    """
    This function returns the most frequent substring from a list of words
    :param words: list[str]: list of words
    """
    frequency = defaultdict(int)

    for word in words:
        for substring in get_substrings(word):
            frequency[substring] += 1

    return max(frequency, key=frequency.get)


### Task2:
def text_to_dict(text: str) -> dict[str, str]:
    """
    This function converts a given text to a dictionary as per the given format
    :param text: str: input text
    """
    result = {}
    keys_vals = text.replace("Additifs nutritionnels : ", "")
    pattern = re.compile(
        r"\S(?<!\d),(?!\d)|–|\s-|\."
    )  # split on , or – or . but not on digits
    for key_value in pattern.split(keys_vals):
        if key_value:
            key, value = list(key_value.split(":"))
            result[key.strip()] = value.strip()
    return result

### Task3:
def number_in_nested_list(l: list, number: int) -> bool:
    """
    This function checks if the given number is present in the nested list
    :param l: list: input list
    :param number: int: number to be checked
    """
    for i in l:
        if isinstance(i, list):
            if number_in_nested_list(i, number):
                return True
        elif i == number:
            return True
    return False

def is_grandma_list(l: list) -> bool:
    """
    This function checks if the given list is a grandma list
    :param l: list: input list
    """
    
    # Check for two consecutive numbers in the list
    for i in range(len(l) - 1):
        if isinstance(l[i], int) and isinstance(l[i + 1], int):
            product = l[i] * l[i + 1]
            if number_in_nested_list(l, product):
                return True

    # Recursively check nested lists
    for item in l:
        if isinstance(item, list):
            if is_grandma_list(item):
                return True

    return False

        


if __name__ == "__main__":
    words = ["milk", "chocolate", "c+", "python", "cat", "dog"]
    # high complexity! would code it like leetcode but this only only for the interview
    # also assume counting substrings that repeat in the same word is allowed
    print("most common substring is", get_most_frequent_substring(words))
    text = "Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %."

    pprint(text_to_dict(text))
        
    # Test examples
    print(is_grandma_list([1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]))  # Output: True
    print(is_grandma_list([5, 9, 4, [[8, 7]], 4, 7, [[5]]]))  # Output: False

