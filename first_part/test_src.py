import unittest
from src import (
    get_substrings,
    get_most_frequent_substring,
    text_to_dict,
    number_in_nested_list,
    is_grandma_list,
)


class TestSrc(unittest.TestCase):

    ### Task1 Tests

    def test_get_substrings(self):
        result = list(get_substrings("test"))
        expected = ["te", "es", "st", "tes", "est", "test"]
        self.assertEqual(result, expected)

    def test_get_most_frequent_substring(self):
        words = ["milk", "chocolate", "c+", "python", "cat", "dog"]
        self.assertEqual(get_most_frequent_substring(words), "ho")

        words = ["abc", "ab", "abcd", "bc"]
        self.assertEqual(get_most_frequent_substring(words), "ab")

        words = ["abc", "ab", "abcd", "bc", "a"]
        self.assertEqual(get_most_frequent_substring(words), "ab")

    ### Task2 Tests

    def test_text_to_dict(self):
        text = "Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg, Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %."
        expected = {
            "Cellulose brute": "0,5 %",
            "Cendres brutes": "1,7 %",
            "Clinoptilolited’origine sédimentaire": "2 g",
            "Cuivre (3b405, 3b406)": "2,2 m",
            "Fer (3b103)": "4m",
            "Humidité": "80,0 %",
            "Iode (3b202)": "0,28 m",
            "Manganèse (3b502, 3b503, 3b504)": "1,1 m",
            "Protéine": "11,0 %",
            "Teneur en matières grasses": "4,5 %",
            "Vitamine C-D3": "160 U",
            "Zinc (3b603,3b605, 3b606)": "11 mg",
        }
        self.assertEqual(text_to_dict(text), expected)

    ### Task3 Tests

    def test_number_in_nested_list(self):
        nested_list = [1, [2, 3], [[4, 5], 6]]
        self.assertTrue(number_in_nested_list(nested_list, 4))
        self.assertFalse(number_in_nested_list(nested_list, 7))
        self.assertTrue(number_in_nested_list(nested_list, 6))

    def test_is_grandma_list(self):
        self.assertTrue(
            is_grandma_list([1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]])
        )  # 1*2=2
        self.assertFalse(is_grandma_list([5, 9, 4, [[8, 7]], 4, 7, [[5]]]))
        self.assertTrue(is_grandma_list([1, 2, 4, [8]]))  # 2*4=8


if __name__ == "__main__":
    unittest.main()
