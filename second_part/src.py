# task1

import requests

def http_request(url = 'https://europe-west1-dataimpact-preproduction.cloudfunctions.net/recruitement_test_requests?task=1'):
    headers = {'Accept': 'application/json'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()  # assuming the response is in JSON format
    else:
        response.raise_for_status()  # raise an exception for HTTP errors
        
# task2
"""
removing the headers from the request seems to work
    >>> curl 'https://europe-west1-dataimpact-preproduction.cloudfunctions.net/recruitement_test_requests?task=2'
    Good job! Now we don't know who you are 🫣
"""
# task3
from datetime import datetime, timedelta
# wrapper to print 'yyyy-mm/d' format
def format_date(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return result.strftime("%Y-%m/%d")
    return wrapper
@format_date
def get_last_monday_month():
    """
    This function returns the last Monday of the month
    the logic is 
    get the last day of the month
    get the week index (monday is 0, sunday is 6) of the day
    subtract the index from the last day of the month to get the last monday of the month
    """
    today = datetime.today()
    last_day_of_month = today.replace(day=1, month=today.month+1) - timedelta(days=1)
    how_many_days_to_subtract = last_day_of_month.weekday() % 7
    last_monday_of_month = last_day_of_month - timedelta(days=how_many_days_to_subtract)
    return last_monday_of_month


if __name__ == "__main__":
    try:
        data = http_request()
        # Response data: {'success': 'Congrats! You have a good headers 😀'}
        print("Response data:", data)
    except requests.exceptions.RequestException as e:
        print(f"HTTP request failed: {e}")
        
    print("Last Monday of the month:", get_last_monday_month())


# task 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

# task 5

class LoginMetaClass(type):
    def __new__(cls, name, bases, dct):
        # iterate over the class attributes
        for method_name, method in dct.items():
            # Check if the attribute is a method
            if callable(method) and method_name != '__init__' and method_name != 'login':
                # Wrap the method with login check
                dct[method_name] = cls.wrap_method(method)

        return super().__new__(cls, name, bases, dct)

    @staticmethod
    def wrap_method(method):
        def wrapper(self, *args, **kwargs):
            if not self.logged_in:
                raise Exception("Access denied. Please log in.")
            return method(self, *args, **kwargs)

        return wrapper

class AccessWebsite(metaclass=LoginMetaClass):
    logged_in = False

    def login(self, username, password):
        if username == "admin" and password == "admin":
            self.logged_in = True

    def access_website(self):
        return "Success"
