import pytest
from src import AccessWebsite,CacheDecorator,http_request
import requests


def test_access_website():
    website = AccessWebsite()
    website.login(username="admin", password="admin")
    assert website.access_website() == "Success"
    website2 = AccessWebsite()
    website2.login(username="test", password="test")
    with pytest.raises(Exception):
        website2.access_website()
        
@pytest.fixture
def cache_decorator():
    return CacheDecorator()


def test_cache_decorator(cache_decorator):
    @cache_decorator
    def add(a, b):
        return a + b

    # should work
    assert add(2, 3) == 5

    # should work
    assert add(3, 3) == 6

    # should be broken because caching only looks at the first argument
    assert add(2, 8) == 10
    
def test_http_request():
    # This test assumes the server is up and running and returns a JSON response
    response = http_request()
    assert isinstance(response, dict)
    assert 'success' in response

def test_http_request_error_handling():
    # Test that the function raises an exception for HTTP errors
    with pytest.raises(requests.HTTPError):
        http_request("https://httpbin.org/status/404")
        

